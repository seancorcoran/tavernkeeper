SLASH_TAVERNKEEPERADDON1 = '/tavern';
local function handler(msg, editbox)
 if msg == 'legendary' then
  PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_legendary.ogg", "SFX");
 elseif msg == 'epic' then
  PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_epic.ogg", "SFX");
 elseif msg == 'rare' then
  PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_rare.ogg", "SFX");
 else
   print("TavernKeeper: Unknown option");
 end
end
SlashCmdList["TAVERNKEEPERADDON"] = handler; -- Also a valid assignment strategy
