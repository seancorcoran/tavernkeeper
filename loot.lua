local Tavern_EventFrame = CreateFrame("FRAME");
Tavern_EventFrame:RegisterEvent("LOOT_OPENED");
local function eventHandler(self, event, ...)
 local numItems = GetNumLootItems();
 if numItems ~= nil then
   local winner = 0;
   local texture, item, quantity, quality, locked = nil;
   for i=1,numItems do
     texture, item, quantity, quality, locked = GetLootSlotInfo(i);
     if quality > winner then
      winner = quality;
     end
   end
   if winner == 3 then
     PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_rare.ogg", "SFX");
   elseif winner == 4 then
     PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_epic.ogg", "SFX");
   elseif winner == 5 then
     PlaySoundFile("Interface\\AddOns\\TavernKeeper\\sounds\\hearthstone_legendary.ogg", "SFX");
   end
 end
end
Tavern_EventFrame:SetScript("OnEvent", eventHandler);
